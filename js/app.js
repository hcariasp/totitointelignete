
/* aqui va el cuerpo del juego inicio*/
/*variables del programa*/
var nombreusuarios;
var numeroClicks = 0;
var turno = 0;
var posicionesUtilizadas     = new Array();
var posicionesOponente       = new Array();
var misPosiciones            = new Array();
var batallasganadasGuardadas = new Array();
var ganador  = new Array();
//matriz de aprendizaje, guardara todos los patrones
//en la cual ganaste al agente
var inteligencia = [ [null, null, null],
                     [null, null, null],
                     [null, null, null],
                     [null, null, null],
                     [null, null, null],
                     [null, null, null],
                     [null, null, null],
                     [null, null, null],
                     [null, null, null],
                     [null, null, null],
                     [null, null, null],
                     [null, null, null],
                     [null, null, null],
                     [null, null, null],
                     [null, null, null],
                     [null, null, null],
                     [null, null, null],
                     [null, null, null],
                     [null, null, null],
                     [null, null, null],
                     [null, null, null],
                     [null, null, null],
                     [null, null, null],
                     [null, null, null],
                     [null, null, null] ];

//inicia el juego
function inicioJuego(){
  quita_ventana_gano();
  //al dar clic dispara la funcion cuerpoJuego
  $('board-container').observe('click', cuerpoJuego);
  $('next-game').observe('click', quita_ventana_gano_reset);
  $('reset').observe('click', quita_ventana_gano_reset);
}

//funcion lo llama el evento click
function cuerpoJuego(event){
    numeroClicks++;
    primerClick(event);
    otrosClick(event);
}


function miusuario() {
  $('myUser').update(nombreusuarios);
}


function primerClick(event) {
  if (numeroClicks == 1) {
      if (validaTurno()) {
          //coloca_x();
          coloca_x(event);
          misPosiciones[extraePosicion()]        = 'X';
          posicionesUtilizadas[extraePosicion()] = 'X';
          if (extraePosicion() == '1') {
              mueveOponente(6);
              posicionesOponente[7]   = 'X';
              posicionesUtilizadas[7] = 'X';
          }else {
            mueveOponente(0);
            posicionesOponente[1]   = 'X';
            posicionesUtilizadas[1] = 'X';
          }
      }
  }
}

function patronesDefensa() {
  var posicionDefensa;
  for (x=0;x<inteligencia.length;x++) {
      if (inteligencia[x][0]!=null) {
        posicionDefensa = validaPatron(inteligencia[x][0], inteligencia[x][1], inteligencia[x][2]);
        if (posicionDefensa!=false) {
           return posicionDefensa;
        }

      }
  }

  for (x=0;x<inteligencia.length;x++) {
      if (inteligencia[x][0]!=null) {
        posicionDefensa = validaPatronUno(inteligencia[x][0], inteligencia[x][1], inteligencia[x][2]);
        if (posicionDefensa!=false) {
           return posicionDefensa;
        }

      }
  }

  for (var i = 1; i < posicionesUtilizadas.length; i++) {

    if (posicionesUtilizadas[i] != 'X') {
      console.log(i);
      return i;
    }
  }
  return false;
}

function validaPatron(x,y,z) {
  if (misPosiciones[x]=='X') {
      if ((misPosiciones[y]=='X') || (misPosiciones[z]=='X') ) {
          if (posicionesUtilizadas[x]!='X') {
             return x;
          }
          if (posicionesUtilizadas[y]!='X') {
             return y;
          }
          if (posicionesUtilizadas[z]!='X') {
             return z;
          }
          return false;
      }
  }
  return false;
}

function validaPatronUno(x,y,z) {
  if ((misPosiciones[x]=='X') || (misPosiciones[y]=='X') || (misPosiciones[z]=='X')){
      if (posicionesUtilizadas[x]!='X') {
          return x;
      }
      if (posicionesUtilizadas[y]!='X') {
          return y;
      }
      if (posicionesUtilizadas[z]!='X') {
         return z;
      }

  }
  return false;
}


function otrosClick(event) {
  if (posicionesUtilizadas[extraePosicion()]!='X') {
    if (numeroClicks > 1) {
        coloca_x(event);
        misPosiciones[extraePosicion()]        = 'X';
        posicionesUtilizadas[extraePosicion()] = 'X';
        ganador = misPosiciones;
        if (buscaGanador()) {
            $('info-partida').show();
        }

        mueveOponente(patronesDefensa()-1);
        posicionesOponente[patronesDefensa()]  = 'X';
        posicionesUtilizadas[patronesDefensa()] = 'X';
        console.log(patronesDefensa());
        //console.log(posicionesOponente);
        //console.log(misPosiciones);
        ganador = posicionesOponente;
        if (buscaGanador()) {
            $('info-partida').show();
        }
    }
  }

}

function buscaGanador() {
  if (ganador[1]=='X') {
      if ((ganador[2]=='X') && (ganador[3]=='X') ) {
          inteligencia[0][0]=1;
          inteligencia[0][1]=2;
          inteligencia[0][2]=3;
          inteligencia[8][0]=3;
          inteligencia[8][1]=2;
          inteligencia[8][2]=1;
          inteligencia[9][0]=2;
          inteligencia[9][1]=1;
          inteligencia[9][2]=3;
          return true;
      }
      if ((ganador[4]=='X') && (ganador[7]=='X') ) {
         inteligencia[1][0]=1;
         inteligencia[1][1]=4;
         inteligencia[1][2]=7;
         inteligencia[10][0]=7;
         inteligencia[10][1]=4;
         inteligencia[10][2]=1;
         inteligencia[23][0]=4;
         inteligencia[23][1]=7;
         inteligencia[23][2]=1;
          return true;
      }
      if ((ganador[5]=='X') && (ganador[9]=='X') ) {
        inteligencia[2][0]=1;
        inteligencia[2][1]=5;
        inteligencia[2][2]=9;
        inteligencia[11][0]=9;
        inteligencia[11][1]=5;
        inteligencia[11][2]=1;
        inteligencia[12][0]=5;
        inteligencia[12][1]=1;
        inteligencia[12][2]=9;
          return true;
      }
  }
  if (ganador[2]=='X') {
    if ((ganador[5]=='X') && (ganador[8]=='X') ) {
          inteligencia[3][0]=2;
          inteligencia[3][1]=5;
          inteligencia[3][2]=8;
          inteligencia[13][0]=8;
          inteligencia[13][1]=5;
          inteligencia[13][2]=2;
          inteligencia[14][0]=5;
          inteligencia[14][1]=2;
          inteligencia[14][2]=8;
          return true;
    }
  }
  if (ganador[3]=='X') {
    if ((ganador[5]=='X') && (ganador[7]=='X') ) {
      inteligencia[4][0]=3;
      inteligencia[4][1]=5;
      inteligencia[4][2]=7;
      inteligencia[15][0]=7;
      inteligencia[15][1]=5;
      inteligencia[15][2]=3;
      inteligencia[16][0]=5;
      inteligencia[16][1]=3;
      inteligencia[16][2]=7;
          return true;
    }
    if ((ganador[6]=='X') && (ganador[9]=='X') ) {
      inteligencia[5][0]=3;
      inteligencia[5][1]=6;
      inteligencia[5][2]=9;
      inteligencia[17][0]=9;
      inteligencia[17][1]=6;
      inteligencia[17][2]=3;
      inteligencia[18][0]=6;
      inteligencia[18][1]=3;
      inteligencia[18][2]=9;
          return true;

    }
  }
  if (ganador[4]=='X') {
    if ((ganador[5]=='X') && (ganador[6]=='X') ) {
      inteligencia[6][0]=4;
      inteligencia[6][1]=5;
      inteligencia[6][2]=6;
      inteligencia[19][0]=6;
      inteligencia[19][1]=5;
      inteligencia[19][2]=4;
      inteligencia[20][0]=5;
      inteligencia[20][1]=4;
      inteligencia[20][2]=6;
          return true;
    }
  }
  if (ganador[7]=='X') {
    if ((ganador[8]=='X') && (ganador[9]=='X') ) {
      inteligencia[7][0]=7;
      inteligencia[7][1]=8;
      inteligencia[7][2]=9;
      inteligencia[21][0]=9;
      inteligencia[21][1]=8;
      inteligencia[21][2]=7;
      inteligencia[22][0]=8;
      inteligencia[22][1]=7;
      inteligencia[22][2]=9;
          return true;
    }

  }
  return false;
}

function mueveOponente(index) {
       var cuadradors = $$('.cuadro');
       cuadradors[index].update('O');
}

function validaTurno(){
    if (turno == 0) {
       return true;
    } else {
      return false;
    }
}

function quita_ventana_gano(){
  $('info-partida').hide();
}

function quita_ventana_gano_reset(){
  $('info-partida').hide();
  limpia_juego();
}

function limpia_juego() {
    var cuadradors = $$('.cuadro');
    //cuadradors[index].update();
    for (var i = 0; i < cuadradors.length; i++) {
      cuadradors[i].update('');
    }

    for (var i = 0; i < posicionesUtilizadas.length; i++) {
      posicionesUtilizadas[i] = '';
    }
    for (var i = 0; i < posicionesOponente.length; i++) {
      posicionesOponente[i] = '';
    }
    for (var i = 0; i < misPosiciones.length; i++) {
      misPosiciones[i] = '';
    }
    for (var i = 0; i < ganador.length; i++) {
      ganador[i] = '';
    }
}

function coloca_x(event){
  var bloqueClickeado = event.findElement();
  bloqueClickeado.down();
  bloqueClickeado.update('X');
}

function coloca_O(event){
  var bloqueClickeado = event.findElement();
  bloqueClickeado.down();
  bloqueClickeado.update('O');
  bloqueClickeado.readAttribute('data-number');
  console.log(bloqueClickeado.text);
  if (bloqueClickeado.readAttribute('data-number')=='6') {
    console.log("entro");
  } else {
    console.log("noentro");
  }
}

function extraePosicion() {
    var bloqueClickeado = event.findElement();
    bloqueClickeado.down();
    return bloqueClickeado.readAttribute('data-number');
}


document.observe("dom:loaded", function(){
  $('modal').hide();
  inicioJuego();
})
/* aqui va el cuerpo del juego fin*/
